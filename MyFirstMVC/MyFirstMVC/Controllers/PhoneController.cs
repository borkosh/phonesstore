﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyFirstMVC.Models;

namespace MyFirstMVC.Controllers
{
    public class PhoneController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _environment;

        public PhoneController(ApplicationDbContext context,
            IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }
        // GET: Phone
        public ActionResult Index()
        {
            IEnumerable<Phone> phones = _context.Phones.OrderBy(s => s.Name);
            return View(phones);
        }

        public IActionResult ManufactRedirect(string url)
        {
            return Redirect(url);
        }


        // GET: Phone/Details/5
        public ActionResult Details(int id)
        {
            Phone phone = _context.Phones.FirstOrDefault(s => s.Id == id);
            if (phone == null)
            {
                return NotFound($"Сорян, телефон с id {id} нет");
            }

            ViewBag.Currencies = _context.Currencies.ToList();

            return View(phone);
        }


        public IActionResult Download(string id)
        {
            try
            {
                string filePath = Path.Combine(_environment.ContentRootPath, $"Files/phone_{id}.pdf");
                string fileType = "application/pdf";
                string fileName = $"phone_{id}.pdf";
                if (!System.IO.File.Exists(filePath))
                {
                    throw new FileNotFoundException($"Описание телефона {id} не найдена");
                }
                return PhysicalFile(filePath, fileType, fileName);
            }
            catch (Exception e)
            {
                ViewData["Message"] = e.Message;
                return View("404");
            }
        }
        // GET: Phone/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Phone/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Phone phone)
        {
            try
            {
                // TODO: Add insert logic here
                _context.Phones.Add(phone);
                _context.SaveChanges();

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Phone/Edit/5
        public ActionResult Edit(Phone phone)
        {

            try
            {
                // TODO: Add insert logic here
                _context.Phones.Update(phone);
                _context.SaveChanges();

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // POST: Phone/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Phone phone)
        {
            try
            {
                _context.Phones.Add(phone);
                _context.SaveChanges();

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Phone/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Phone/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}