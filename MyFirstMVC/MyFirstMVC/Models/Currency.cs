﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace MyFirstMVC.Models
{
    public class Currency
    {
        public int Id { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencyName { get; set; }
        public double CurrencyRate { get; set; }

        public double Calc(double a,double b)
        {
            double result = a * b;
            return result;
        }

    }
}
